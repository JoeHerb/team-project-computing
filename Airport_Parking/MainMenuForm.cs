﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Airport_Parking
{
    public partial class MainMenuForm : Form
    {
        private BookingMenuForm bookingMenuForm;
        private CustomerMenuForm customerMenuForm;

        public MainMenuForm()
        {
            InitializeComponent();
        }

        //Opens booking menu form
        private void bookingButton_Click(object sender, EventArgs e)
        {
            bookingMenuForm = new BookingMenuForm();
            bookingMenuForm.Show();
            this.Hide();
        }

        //Opens customer menu form
        private void customerButton_Click(object sender, EventArgs e)
        {
            customerMenuForm = new CustomerMenuForm();
            customerMenuForm.Show();
            this.Hide();
        }

        //Exits application when close button is pressed
        private void MainMenuForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
