﻿namespace Airport_Parking
{
    partial class NewCustomerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewCustomerForm));
            this.forenameLabel = new System.Windows.Forms.Label();
            this.surnameLabel = new System.Windows.Forms.Label();
            this.addressLabel = new System.Windows.Forms.Label();
            this.telNoLabel = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.forenameMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.surnameMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.phoneNoMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.registerButton = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.menuButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // forenameLabel
            // 
            this.forenameLabel.AutoSize = true;
            this.forenameLabel.Location = new System.Drawing.Point(50, 45);
            this.forenameLabel.Name = "forenameLabel";
            this.forenameLabel.Size = new System.Drawing.Size(57, 13);
            this.forenameLabel.TabIndex = 0;
            this.forenameLabel.Text = "Forename:";
            // 
            // surnameLabel
            // 
            this.surnameLabel.AutoSize = true;
            this.surnameLabel.Location = new System.Drawing.Point(50, 78);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(52, 13);
            this.surnameLabel.TabIndex = 1;
            this.surnameLabel.Text = "Surname:";
            // 
            // addressLabel
            // 
            this.addressLabel.AutoSize = true;
            this.addressLabel.Location = new System.Drawing.Point(49, 112);
            this.addressLabel.Name = "addressLabel";
            this.addressLabel.Size = new System.Drawing.Size(48, 13);
            this.addressLabel.TabIndex = 2;
            this.addressLabel.Text = "Address:";
            // 
            // telNoLabel
            // 
            this.telNoLabel.AutoSize = true;
            this.telNoLabel.Location = new System.Drawing.Point(49, 170);
            this.telNoLabel.Name = "telNoLabel";
            this.telNoLabel.Size = new System.Drawing.Size(58, 13);
            this.telNoLabel.TabIndex = 3;
            this.telNoLabel.Text = "Phone No:";
            // 
            // emailLabel
            // 
            this.emailLabel.AutoSize = true;
            this.emailLabel.Location = new System.Drawing.Point(49, 196);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(39, 13);
            this.emailLabel.TabIndex = 4;
            this.emailLabel.Text = "E-Mail:";
            // 
            // forenameMaskedTextBox
            // 
            this.forenameMaskedTextBox.Location = new System.Drawing.Point(141, 42);
            this.forenameMaskedTextBox.Mask = "LLLLLLLLLLLLLLL";
            this.forenameMaskedTextBox.Name = "forenameMaskedTextBox";
            this.forenameMaskedTextBox.Size = new System.Drawing.Size(104, 20);
            this.forenameMaskedTextBox.TabIndex = 5;
            // 
            // surnameMaskedTextBox
            // 
            this.surnameMaskedTextBox.Location = new System.Drawing.Point(142, 75);
            this.surnameMaskedTextBox.Mask = "LLLLLLLLLLLLLLL";
            this.surnameMaskedTextBox.Name = "surnameMaskedTextBox";
            this.surnameMaskedTextBox.Size = new System.Drawing.Size(104, 20);
            this.surnameMaskedTextBox.TabIndex = 6;
            // 
            // phoneNoMaskedTextBox
            // 
            this.phoneNoMaskedTextBox.Location = new System.Drawing.Point(142, 167);
            this.phoneNoMaskedTextBox.Mask = "00000000000";
            this.phoneNoMaskedTextBox.Name = "phoneNoMaskedTextBox";
            this.phoneNoMaskedTextBox.Size = new System.Drawing.Size(104, 20);
            this.phoneNoMaskedTextBox.TabIndex = 7;
            // 
            // addressTextBox
            // 
            this.addressTextBox.Location = new System.Drawing.Point(143, 109);
            this.addressTextBox.Multiline = true;
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.Size = new System.Drawing.Size(103, 52);
            this.addressTextBox.TabIndex = 8;
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(142, 193);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(103, 20);
            this.emailTextBox.TabIndex = 9;
            // 
            // registerButton
            // 
            this.registerButton.Location = new System.Drawing.Point(92, 222);
            this.registerButton.Name = "registerButton";
            this.registerButton.Size = new System.Drawing.Size(91, 27);
            this.registerButton.TabIndex = 10;
            this.registerButton.Text = "Register";
            this.registerButton.UseVisualStyleBackColor = true;
            this.registerButton.Click += new System.EventHandler(this.registerButton_Click);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // menuButton
            // 
            this.menuButton.Location = new System.Drawing.Point(12, 12);
            this.menuButton.Name = "menuButton";
            this.menuButton.Size = new System.Drawing.Size(48, 23);
            this.menuButton.TabIndex = 16;
            this.menuButton.Text = "Menu";
            this.menuButton.UseVisualStyleBackColor = true;
            this.menuButton.Click += new System.EventHandler(this.menuButton_Click);
            // 
            // NewCustomerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.menuButton);
            this.Controls.Add(this.registerButton);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.addressTextBox);
            this.Controls.Add(this.phoneNoMaskedTextBox);
            this.Controls.Add(this.surnameMaskedTextBox);
            this.Controls.Add(this.forenameMaskedTextBox);
            this.Controls.Add(this.emailLabel);
            this.Controls.Add(this.telNoLabel);
            this.Controls.Add(this.addressLabel);
            this.Controls.Add(this.surnameLabel);
            this.Controls.Add(this.forenameLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewCustomerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Airport Parking - Customer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.NewCustomerForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label forenameLabel;
        private System.Windows.Forms.Label surnameLabel;
        private System.Windows.Forms.Label addressLabel;
        private System.Windows.Forms.Label telNoLabel;
        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.MaskedTextBox forenameMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox surnameMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox phoneNoMaskedTextBox;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Button registerButton;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Button menuButton;
    }
}