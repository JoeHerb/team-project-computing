﻿namespace Airport_Parking
{
    partial class AmmendBookingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AmmendBookingForm));
            this.menuButton = new System.Windows.Forms.Button();
            this.startDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.regNoMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.customerIDMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.startDateLabel = new System.Windows.Forms.Label();
            this.regNoLabel = new System.Windows.Forms.Label();
            this.customerIDLabel = new System.Windows.Forms.Label();
            this.bookingDetailsGroupBox = new System.Windows.Forms.GroupBox();
            this.deleteBookingButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.leaveDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.leaveLabel = new System.Windows.Forms.Label();
            this.spaceTypeComboBox = new System.Windows.Forms.ComboBox();
            this.spaceTypeLabel = new System.Windows.Forms.Label();
            this.searchRecordsButton = new System.Windows.Forms.Button();
            this.priceLabel = new System.Windows.Forms.Label();
            this.bookingDetailsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuButton
            // 
            this.menuButton.Location = new System.Drawing.Point(12, 12);
            this.menuButton.Name = "menuButton";
            this.menuButton.Size = new System.Drawing.Size(48, 23);
            this.menuButton.TabIndex = 17;
            this.menuButton.Text = "Menu";
            this.menuButton.UseVisualStyleBackColor = true;
            this.menuButton.Click += new System.EventHandler(this.menuButton_Click);
            // 
            // startDateTimePicker
            // 
            this.startDateTimePicker.Location = new System.Drawing.Point(188, 68);
            this.startDateTimePicker.Name = "startDateTimePicker";
            this.startDateTimePicker.Size = new System.Drawing.Size(102, 20);
            this.startDateTimePicker.TabIndex = 20;
            // 
            // regNoMaskedTextBox
            // 
            this.regNoMaskedTextBox.Location = new System.Drawing.Point(188, 42);
            this.regNoMaskedTextBox.Mask = "LL00LLL";
            this.regNoMaskedTextBox.Name = "regNoMaskedTextBox";
            this.regNoMaskedTextBox.Size = new System.Drawing.Size(103, 20);
            this.regNoMaskedTextBox.TabIndex = 19;
            // 
            // customerIDMaskedTextBox
            // 
            this.customerIDMaskedTextBox.Location = new System.Drawing.Point(188, 16);
            this.customerIDMaskedTextBox.Mask = "000000000";
            this.customerIDMaskedTextBox.Name = "customerIDMaskedTextBox";
            this.customerIDMaskedTextBox.Size = new System.Drawing.Size(103, 20);
            this.customerIDMaskedTextBox.TabIndex = 18;
            // 
            // startDateLabel
            // 
            this.startDateLabel.AutoSize = true;
            this.startDateLabel.Location = new System.Drawing.Point(86, 75);
            this.startDateLabel.Name = "startDateLabel";
            this.startDateLabel.Size = new System.Drawing.Size(58, 13);
            this.startDateLabel.TabIndex = 23;
            this.startDateLabel.Text = "Start Date:";
            // 
            // regNoLabel
            // 
            this.regNoLabel.AutoSize = true;
            this.regNoLabel.Location = new System.Drawing.Point(86, 49);
            this.regNoLabel.Name = "regNoLabel";
            this.regNoLabel.Size = new System.Drawing.Size(47, 13);
            this.regNoLabel.TabIndex = 22;
            this.regNoLabel.Text = "Reg No:";
            // 
            // customerIDLabel
            // 
            this.customerIDLabel.AutoSize = true;
            this.customerIDLabel.Location = new System.Drawing.Point(85, 19);
            this.customerIDLabel.Name = "customerIDLabel";
            this.customerIDLabel.Size = new System.Drawing.Size(68, 13);
            this.customerIDLabel.TabIndex = 21;
            this.customerIDLabel.Text = "Customer ID:";
            // 
            // bookingDetailsGroupBox
            // 
            this.bookingDetailsGroupBox.Controls.Add(this.priceLabel);
            this.bookingDetailsGroupBox.Controls.Add(this.deleteBookingButton);
            this.bookingDetailsGroupBox.Controls.Add(this.updateButton);
            this.bookingDetailsGroupBox.Controls.Add(this.leaveDateTimePicker);
            this.bookingDetailsGroupBox.Controls.Add(this.leaveLabel);
            this.bookingDetailsGroupBox.Controls.Add(this.spaceTypeComboBox);
            this.bookingDetailsGroupBox.Controls.Add(this.spaceTypeLabel);
            this.bookingDetailsGroupBox.Enabled = false;
            this.bookingDetailsGroupBox.Location = new System.Drawing.Point(59, 112);
            this.bookingDetailsGroupBox.Name = "bookingDetailsGroupBox";
            this.bookingDetailsGroupBox.Size = new System.Drawing.Size(282, 176);
            this.bookingDetailsGroupBox.TabIndex = 24;
            this.bookingDetailsGroupBox.TabStop = false;
            this.bookingDetailsGroupBox.Text = "Booking Details";
            this.bookingDetailsGroupBox.Visible = false;
            // 
            // deleteBookingButton
            // 
            this.deleteBookingButton.Location = new System.Drawing.Point(154, 99);
            this.deleteBookingButton.Name = "deleteBookingButton";
            this.deleteBookingButton.Size = new System.Drawing.Size(78, 25);
            this.deleteBookingButton.TabIndex = 30;
            this.deleteBookingButton.Text = "Delete";
            this.deleteBookingButton.UseVisualStyleBackColor = true;
            this.deleteBookingButton.Click += new System.EventHandler(this.deleteBookingButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(48, 99);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(78, 25);
            this.updateButton.TabIndex = 29;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // leaveDateTimePicker
            // 
            this.leaveDateTimePicker.Location = new System.Drawing.Point(130, 29);
            this.leaveDateTimePicker.Name = "leaveDateTimePicker";
            this.leaveDateTimePicker.Size = new System.Drawing.Size(102, 20);
            this.leaveDateTimePicker.TabIndex = 28;
            this.leaveDateTimePicker.ValueChanged += new System.EventHandler(this.leaveDateTimePicker_ValueChanged);
            // 
            // leaveLabel
            // 
            this.leaveLabel.AutoSize = true;
            this.leaveLabel.Location = new System.Drawing.Point(34, 36);
            this.leaveLabel.Name = "leaveLabel";
            this.leaveLabel.Size = new System.Drawing.Size(66, 13);
            this.leaveLabel.TabIndex = 27;
            this.leaveLabel.Text = "Leave Date:";
            // 
            // spaceTypeComboBox
            // 
            this.spaceTypeComboBox.FormattingEnabled = true;
            this.spaceTypeComboBox.Items.AddRange(new object[] {
            "Standard",
            "Luxury",
            "Extra Secure"});
            this.spaceTypeComboBox.Location = new System.Drawing.Point(130, 55);
            this.spaceTypeComboBox.Name = "spaceTypeComboBox";
            this.spaceTypeComboBox.Size = new System.Drawing.Size(102, 21);
            this.spaceTypeComboBox.TabIndex = 25;
            this.spaceTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.spaceTypeComboBox_SelectedIndexChanged);
            // 
            // spaceTypeLabel
            // 
            this.spaceTypeLabel.AutoSize = true;
            this.spaceTypeLabel.Location = new System.Drawing.Point(36, 63);
            this.spaceTypeLabel.Name = "spaceTypeLabel";
            this.spaceTypeLabel.Size = new System.Drawing.Size(65, 13);
            this.spaceTypeLabel.TabIndex = 26;
            this.spaceTypeLabel.Text = "Space Type";
            // 
            // searchRecordsButton
            // 
            this.searchRecordsButton.Location = new System.Drawing.Point(154, 309);
            this.searchRecordsButton.Name = "searchRecordsButton";
            this.searchRecordsButton.Size = new System.Drawing.Size(75, 23);
            this.searchRecordsButton.TabIndex = 25;
            this.searchRecordsButton.Text = "Search";
            this.searchRecordsButton.UseVisualStyleBackColor = true;
            this.searchRecordsButton.Click += new System.EventHandler(this.searchRecordsButton_Click);
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(92, 143);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(76, 13);
            this.priceLabel.TabIndex = 26;
            this.priceLabel.Text = "Total Price: £0";
            // 
            // AmmendBookingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 344);
            this.Controls.Add(this.searchRecordsButton);
            this.Controls.Add(this.bookingDetailsGroupBox);
            this.Controls.Add(this.startDateLabel);
            this.Controls.Add(this.regNoLabel);
            this.Controls.Add(this.customerIDLabel);
            this.Controls.Add(this.startDateTimePicker);
            this.Controls.Add(this.regNoMaskedTextBox);
            this.Controls.Add(this.customerIDMaskedTextBox);
            this.Controls.Add(this.menuButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AmmendBookingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Airport Parking - Booking";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AmmendBookingForm_FormClosed);
            this.bookingDetailsGroupBox.ResumeLayout(false);
            this.bookingDetailsGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button menuButton;
        private System.Windows.Forms.DateTimePicker startDateTimePicker;
        private System.Windows.Forms.MaskedTextBox regNoMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox customerIDMaskedTextBox;
        private System.Windows.Forms.Label startDateLabel;
        private System.Windows.Forms.Label regNoLabel;
        private System.Windows.Forms.Label customerIDLabel;
        private System.Windows.Forms.GroupBox bookingDetailsGroupBox;
        private System.Windows.Forms.DateTimePicker leaveDateTimePicker;
        private System.Windows.Forms.Label leaveLabel;
        private System.Windows.Forms.Label spaceTypeLabel;
        private System.Windows.Forms.ComboBox spaceTypeComboBox;
        private System.Windows.Forms.Button deleteBookingButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Button searchRecordsButton;
        private System.Windows.Forms.Label priceLabel;
    }
}