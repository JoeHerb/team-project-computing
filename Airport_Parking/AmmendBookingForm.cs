﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Airport_Parking
{
    public partial class AmmendBookingForm : Form
    {
        private int totalPrice;
        private int assignedSpace;
        private bool valet;
        private MainMenuForm mainMenuForm;

        OleDbConnection myConn = new OleDbConnection();

        public AmmendBookingForm()
        {
            InitializeComponent();
        }

        private void AmmendBookingForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void menuButton_Click(object sender, EventArgs e)
        {
            mainMenuForm = new MainMenuForm();
            mainMenuForm.Show();
            this.Hide();
        }

        private void searchRecordsButton_Click(object sender, EventArgs e)
        {
            myConn.ConnectionString = connectionDetails.dbsource;
            OleDbCommand myCmd =  myConn.CreateCommand();
            OleDbCommand myCmd2 = myConn.CreateCommand();

            myCmd.CommandText = @"Select LeaveDate, SpaceID
                                  From Booking
                                  Where CustomerID=@customerID AND BookingDate=@bookingdate AND RegNo=@regNo";
            myCmd.Parameters.AddWithValue("customerID", customerIDMaskedTextBox.Text);
            myCmd.Parameters.AddWithValue("bookingDate", startDateTimePicker.Value.Date);
            myCmd.Parameters.AddWithValue("regNo", regNoMaskedTextBox.Text);

            myConn.Open();
            OleDbDataReader myDR = myCmd.ExecuteReader();

            if (myDR.HasRows)
            {
                myDR.Read();


                myCmd2.CommandText = @"Select SpaceType
                                       From DesignatedSpace
                                       Where SpaceID=@spaceID";
                myCmd2.Parameters.AddWithValue("spaceID", Convert.ToString(myDR["SpaceID"]));

                OleDbDataReader myDR2 = myCmd2.ExecuteReader();

                if (myDR2.HasRows)
                {
                    myDR2.Read();
                    leaveDateTimePicker.Value = Convert.ToDateTime(myDR["LeaveDate"]);
                    spaceTypeComboBox.Text = Convert.ToString(myDR2["SpaceType"]);
                    bookingDetailsGroupBox.Enabled = true;
                    bookingDetailsGroupBox.Visible = true;
                    customerIDMaskedTextBox.Enabled = false;
                    startDateTimePicker.Enabled = false;
                    regNoMaskedTextBox.Enabled = false;
                    searchRecordsButton.Enabled = false;
                }
                else
                {
                    MessageBox.Show("No Spaces of thet type exist.");
                    myDR2.Close();
                }
            }
            else 
            {
                MessageBox.Show("No records of this booking exist, please try again.");
                myDR.Close();
                bookingDetailsGroupBox.Enabled = false;
                bookingDetailsGroupBox.Visible = false;
            }

            myConn.Close();
        }

        private void deleteBookingButton_Click(object sender, EventArgs e)
        {
            myConn.ConnectionString = connectionDetails.dbsource;

            OleDbCommand myCmd = myConn.CreateCommand();

            myCmd.CommandText = @"Delete From Booking
                                  Where CustomerID=@customerID AND BookingDate=@bookingDate AND RegNo=@regNo";
            myCmd.Parameters.AddWithValue("customerID", customerIDMaskedTextBox.Text);
            myCmd.Parameters.AddWithValue("bookingDate", startDateTimePicker.Value.Date);
            myCmd.Parameters.AddWithValue("regNo", regNoMaskedTextBox.Text);

            try
            {
                myConn.Open();
                myCmd.ExecuteNonQuery();
                myConn.Close();

                bookingDetailsGroupBox.Visible = false;
                bookingDetailsGroupBox.Enabled = false;

                customerIDMaskedTextBox.Enabled = true;
                customerIDMaskedTextBox.Text = "";
                regNoMaskedTextBox.Enabled = true;
                regNoMaskedTextBox.Text = "";
                startDateTimePicker.Enabled = true;
                startDateTimePicker.Value = DateTime.Now;

                MessageBox.Show("The booking has been deleted.");
            }
            catch
            {
                MessageBox.Show("There was an error cancelling the booking, please try again.");
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            bool confirmedBooking;

            assignedSpace = getFreeSpace();

            if (assignedSpace != 0)
            {
                confirmedBooking = updateBooking();

                if (confirmedBooking == true)
                {
                    MessageBox.Show("Successfully Updated Booking.");
                    bookingDetailsGroupBox.Enabled = false;
                    bookingDetailsGroupBox.Visible = false;
                    customerIDMaskedTextBox.Text = "";
                    customerIDMaskedTextBox.Enabled = true;
                    regNoMaskedTextBox.Text = "";
                    regNoMaskedTextBox.Enabled = true;
                    startDateTimePicker.Enabled = true;
                    startDateTimePicker.Value = DateTime.Now;
                    searchRecordsButton.Enabled = true;
                    
                }
                else
                    MessageBox.Show("There was an error updating booking, please try again.");
            }
            else
                MessageBox.Show("There was an error allocating space, please try again.");
        }

        private void leaveDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            setPrice();
        }

        private void spaceTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            setPrice();
        }

        private void setPrice()
        {
            int dailyPrice = 0;

            if (spaceTypeComboBox.Text == "Standard")
            {
                dailyPrice = 6;
                valet = false;
            }
            else if (spaceTypeComboBox.Text == "Luxury")
            {
                dailyPrice = 7;
                valet = true;
            }
            else if (spaceTypeComboBox.Text == "Extra Secure")
            {
                dailyPrice = 8;
                valet = false;
            }

            TimeSpan duration = leaveDateTimePicker.Value - startDateTimePicker.Value;

            totalPrice = (dailyPrice * duration.Days);
            priceLabel.Text = "Total Price: £" + totalPrice;
        }

        private bool updateBooking()
        {
            try
            {
                myConn.ConnectionString = connectionDetails.dbsource;
                OleDbCommand myCmd = myConn.CreateCommand();

                myCmd.CommandText = @"Update Booking Set SpaceID=@spaceID, LeaveDate=@leaveDate, TotalCost=@totalCost, Valet=@valet Where CustomerID=@customerID AND BookingDate=@bookingDate AND RegNo=@regNo";

                myCmd.Parameters.AddWithValue("spaceID", assignedSpace);
                myCmd.Parameters.AddWithValue("leaveDate", leaveDateTimePicker.Value.Date);
                myCmd.Parameters.AddWithValue("totalCost", totalPrice);
                myCmd.Parameters.AddWithValue("valet", valet);
                myCmd.Parameters.AddWithValue("customerID", customerIDMaskedTextBox.Text);
                myCmd.Parameters.AddWithValue("bookingDate", startDateTimePicker.Value.Date);
                myCmd.Parameters.AddWithValue("regNo", regNoMaskedTextBox.Text);
                
                myConn.Open();
                myCmd.ExecuteNonQuery();
                myConn.Close();

                return true;
            }
            catch
            {
                MessageBox.Show("There was a problem in placeBooking");
                myConn.Close();
                return false;
            }
        }

        private int getFreeSpace()
        {
            int space = 0;

            myConn.ConnectionString = connectionDetails.dbsource;

            OleDbCommand myCmd = myConn.CreateCommand();
            OleDbCommand myCmd2 = myConn.CreateCommand();
            OleDbCommand myCmd3 = myConn.CreateCommand();

            if (spaceTypeComboBox.SelectedIndex == 0)
                spaceTypeComboBox.Text = "Standard";
            else if (spaceTypeComboBox.SelectedIndex == 1)
                spaceTypeComboBox.Text = "Luxury";
            else if (spaceTypeComboBox.SelectedIndex == 2)
                spaceTypeComboBox.Text = "Extra Secure";

            myCmd.CommandText = @"SELECT [SpaceID]
                                FROM DesignatedSpace
                                WHERE [SpaceType]=@spaceType
                                AND [SpaceID] NOT IN (SELECT [SpaceID]
                                                      FROM [Booking])";

            myCmd.Parameters.AddWithValue("spaceType", spaceTypeComboBox.Text);

            myConn.Open();
            OleDbDataReader myDR = myCmd.ExecuteReader();

            if (myDR.HasRows)
            {
                myDR.Read();
                space = Convert.ToInt16(myDR["SpaceID"]);
                myDR.Close();
                myConn.Close();
                return space;
            }

            myCmd2.CommandText = @"SELECT [SpaceID]
                                FROM DesignatedSpace
                                WHERE [SpaceType]=@spaceType";

            myCmd2.Parameters.AddWithValue("spaceType", spaceTypeComboBox.Text);
            myCmd2.Parameters.AddWithValue("startDate", startDateTimePicker.Value.Date);
            myCmd2.Parameters.AddWithValue("leaveDate", leaveDateTimePicker.Value.Date);

            OleDbDataReader myDR2 = myCmd2.ExecuteReader();
            myDR2.Read();

            while (myDR2.HasRows)
            {
                try
                {
                    myCmd3.CommandText = @"SELECT [SpaceID]
                                       FROM [Booking]
                                       WHERE [SpaceID]=@spaceID
                                       AND [BookingDate] < @startDate OR @leaveDate < [BookingDate]
                                       AND [LeaveDate] < @startDate OR @leaveDate < [BookingDate]";

                    myCmd3.Parameters.AddWithValue("spaceID", myDR2["SpaceID"]);
                    myCmd3.Parameters.AddWithValue("startDate", startDateTimePicker.Value.Date);
                    myCmd3.Parameters.AddWithValue("leaveDate", leaveDateTimePicker.Value.Date);

                    OleDbDataReader myDR3 = myCmd3.ExecuteReader();

                    if (myDR3.HasRows)
                    {
                        myDR3.Read();
                        space = Convert.ToInt16(myDR3["SpaceID"]);
                        myDR3.Close();
                        myConn.Close();
                        return space;
                    }
                    else
                    {
                        myDR3.Close();
                        myConn.Close();
                    }

                    myDR2.Read();
                }
                catch
                {
                    MessageBox.Show("There were no spaces available");
                }
            }

            myConn.Close();
            return space;
        }

    }
}
