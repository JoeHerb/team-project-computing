﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Net.Mail;

namespace Airport_Parking
{
    public partial class AmmendCustomerDetailsForm : Form
    {
        private MainMenuForm mainMenuform;

        OleDbConnection myConn = new OleDbConnection();
        
        public AmmendCustomerDetailsForm()
        {
            InitializeComponent();
        }

        private void AmmendCustomerDetailsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void searchRecordsButton_Click(object sender, EventArgs e)
        {
            myConn.ConnectionString = connectionDetails.dbsource;
            OleDbCommand myCmd = myConn.CreateCommand();

            myCmd.CommandText = "Select Forename, Surname, Address, TelephoneNo, EMail From Customer"
                                + " Where CustomerID=@custID";

            myCmd.Parameters.AddWithValue("custID", customerIDMaskedTextBox.Text);

            myConn.Open();
            OleDbDataReader myDR = myCmd.ExecuteReader();

            if (myDR.HasRows)
            {
                errorProvider.SetError(customerIDMaskedTextBox, String.Empty);
                myDR.Read();

                forenameMaskedTextBox.Text = Convert.ToString(myDR["Forename"]);
                surnameMaskedTextBox.Text = Convert.ToString(myDR["Surname"]);
                addressTextBox.Text = Convert.ToString(myDR["Address"]);
                phoneNoMaskedTextBox.Text = Convert.ToString(myDR["TelephoneNo"]);
                emailTextBox.Text = Convert.ToString(myDR["EMail"]);

                myConn.Close();

                customerDetailsGroupBox.Enabled = true;
                customerDetailsGroupBox.Visible = true;
                customerIDMaskedTextBox.Enabled = false;

                searchRecordsButton.Enabled = false;
                updateButton.Enabled = true;
            }

            else
            {
                errorProvider.SetError(customerIDMaskedTextBox, "No matching ID found, please try again.");
                myConn.Close();
            }
        }
        
        private void updateButton_Click(object sender, EventArgs e)
        {
            myConn.ConnectionString = connectionDetails.dbsource;
            OleDbCommand myCmd = myConn.CreateCommand();

            myCmd.CommandText = "Update Customer Set Forename=@forename, Surname=@surname, Address=@address, TelephoneNo=@phoneNo, EMail=@email Where CustomerID=@customerID";

            myCmd.Parameters.AddWithValue("forename", forenameMaskedTextBox.Text);
            myCmd.Parameters.AddWithValue("surname", surnameMaskedTextBox.Text);
            myCmd.Parameters.AddWithValue("address", addressTextBox.Text);
            myCmd.Parameters.AddWithValue("phoneNo", phoneNoMaskedTextBox.Text);
            myCmd.Parameters.AddWithValue("email", emailTextBox.Text);
            myCmd.Parameters.AddWithValue("customerID", customerIDMaskedTextBox.Text);

            myConn.Open();
            OleDbDataReader myDR = myCmd.ExecuteReader();
            myConn.Close();

            updateButton.Enabled = false;
            searchRecordsButton.Enabled = true;
            customerDetailsGroupBox.Visible = false;
            customerDetailsGroupBox.Enabled = false;
            customerIDMaskedTextBox.Enabled = true;
        }

        private void menuButton_Click(object sender, EventArgs e)
        {
            mainMenuform = new MainMenuForm();
            mainMenuform.Show();
            this.Hide();
        }

        private void deleteCustomerButton_Click(object sender, EventArgs e)
        {
            myConn.ConnectionString = connectionDetails.dbsource;
            OleDbCommand myCmd = myConn.CreateCommand();
            OleDbCommand myCmd2 = myConn.CreateCommand();

            myCmd.CommandText = @"Delete From Customer
                                  Where CustomerID=@customerID";
            myCmd.Parameters.AddWithValue("customerID", customerIDMaskedTextBox.Text);

            myCmd2.CommandText = @"Delete From Booking
                                   Where CustomerID=@customerID";
            myCmd2.Parameters.AddWithValue("customerID", customerIDMaskedTextBox.Text);

            try
            {
                myConn.Open();
                myCmd.ExecuteNonQuery();
                myCmd2.ExecuteNonQuery();
                myConn.Close();

                customerDetailsGroupBox.Visible = false;
                customerDetailsGroupBox.Enabled = false;

                customerIDMaskedTextBox.Enabled = true;
                customerIDMaskedTextBox.Text = "";

                searchRecordsButton.Enabled = true;

            }
            catch
            {
                MessageBox.Show("There was an error removing the customer, please try again.");
            }
        }

        private bool CheckEmail()
        {
            try
            {
                MailAddress m = new MailAddress(emailTextBox.Text);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
