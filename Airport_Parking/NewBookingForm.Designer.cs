﻿namespace Airport_Parking
{
    partial class NewBookingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewBookingForm));
            this.customerIDMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.customerIDLabel = new System.Windows.Forms.Label();
            this.regNoLabel = new System.Windows.Forms.Label();
            this.regNoMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.startDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.startDateLabel = new System.Windows.Forms.Label();
            this.spaceTypeComboBox = new System.Windows.Forms.ComboBox();
            this.spaceTypeLabel = new System.Windows.Forms.Label();
            this.leaveLabel = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            this.bookButton = new System.Windows.Forms.Button();
            this.leaveDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.menuButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // customerIDMaskedTextBox
            // 
            this.customerIDMaskedTextBox.Location = new System.Drawing.Point(153, 42);
            this.customerIDMaskedTextBox.Mask = "000000000";
            this.customerIDMaskedTextBox.Name = "customerIDMaskedTextBox";
            this.customerIDMaskedTextBox.Size = new System.Drawing.Size(103, 20);
            this.customerIDMaskedTextBox.TabIndex = 2;
            // 
            // customerIDLabel
            // 
            this.customerIDLabel.AutoSize = true;
            this.customerIDLabel.Location = new System.Drawing.Point(56, 45);
            this.customerIDLabel.Name = "customerIDLabel";
            this.customerIDLabel.Size = new System.Drawing.Size(68, 13);
            this.customerIDLabel.TabIndex = 3;
            this.customerIDLabel.Text = "Customer ID:";
            // 
            // regNoLabel
            // 
            this.regNoLabel.AutoSize = true;
            this.regNoLabel.Location = new System.Drawing.Point(57, 75);
            this.regNoLabel.Name = "regNoLabel";
            this.regNoLabel.Size = new System.Drawing.Size(47, 13);
            this.regNoLabel.TabIndex = 4;
            this.regNoLabel.Text = "Reg No:";
            // 
            // regNoMaskedTextBox
            // 
            this.regNoMaskedTextBox.Location = new System.Drawing.Point(153, 68);
            this.regNoMaskedTextBox.Mask = "LL00LLL";
            this.regNoMaskedTextBox.Name = "regNoMaskedTextBox";
            this.regNoMaskedTextBox.Size = new System.Drawing.Size(103, 20);
            this.regNoMaskedTextBox.TabIndex = 5;
            // 
            // startDateTimePicker
            // 
            this.startDateTimePicker.Location = new System.Drawing.Point(153, 94);
            this.startDateTimePicker.Name = "startDateTimePicker";
            this.startDateTimePicker.Size = new System.Drawing.Size(102, 20);
            this.startDateTimePicker.TabIndex = 6;
            // 
            // startDateLabel
            // 
            this.startDateLabel.AutoSize = true;
            this.startDateLabel.Location = new System.Drawing.Point(57, 101);
            this.startDateLabel.Name = "startDateLabel";
            this.startDateLabel.Size = new System.Drawing.Size(58, 13);
            this.startDateLabel.TabIndex = 7;
            this.startDateLabel.Text = "Start Date:";
            // 
            // spaceTypeComboBox
            // 
            this.spaceTypeComboBox.FormattingEnabled = true;
            this.spaceTypeComboBox.Items.AddRange(new object[] {
            "Standard",
            "Luxury",
            "Extra Secure"});
            this.spaceTypeComboBox.Location = new System.Drawing.Point(153, 146);
            this.spaceTypeComboBox.Name = "spaceTypeComboBox";
            this.spaceTypeComboBox.Size = new System.Drawing.Size(102, 21);
            this.spaceTypeComboBox.TabIndex = 8;
            this.spaceTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.spaceTypeComboBox_SelectedIndexChanged);
            // 
            // spaceTypeLabel
            // 
            this.spaceTypeLabel.AutoSize = true;
            this.spaceTypeLabel.Location = new System.Drawing.Point(59, 154);
            this.spaceTypeLabel.Name = "spaceTypeLabel";
            this.spaceTypeLabel.Size = new System.Drawing.Size(65, 13);
            this.spaceTypeLabel.TabIndex = 9;
            this.spaceTypeLabel.Text = "Space Type";
            // 
            // leaveLabel
            // 
            this.leaveLabel.AutoSize = true;
            this.leaveLabel.Location = new System.Drawing.Point(57, 127);
            this.leaveLabel.Name = "leaveLabel";
            this.leaveLabel.Size = new System.Drawing.Size(66, 13);
            this.leaveLabel.TabIndex = 10;
            this.leaveLabel.Text = "Leave Date:";
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(107, 194);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(76, 13);
            this.priceLabel.TabIndex = 12;
            this.priceLabel.Text = "Total Price: £0";
            // 
            // bookButton
            // 
            this.bookButton.Location = new System.Drawing.Point(89, 221);
            this.bookButton.Name = "bookButton";
            this.bookButton.Size = new System.Drawing.Size(107, 28);
            this.bookButton.TabIndex = 13;
            this.bookButton.Text = "Book";
            this.bookButton.UseVisualStyleBackColor = true;
            this.bookButton.Click += new System.EventHandler(this.bookButton_Click);
            // 
            // leaveDateTimePicker
            // 
            this.leaveDateTimePicker.Location = new System.Drawing.Point(153, 120);
            this.leaveDateTimePicker.Name = "leaveDateTimePicker";
            this.leaveDateTimePicker.Size = new System.Drawing.Size(102, 20);
            this.leaveDateTimePicker.TabIndex = 14;
            // 
            // menuButton
            // 
            this.menuButton.Location = new System.Drawing.Point(12, 12);
            this.menuButton.Name = "menuButton";
            this.menuButton.Size = new System.Drawing.Size(48, 23);
            this.menuButton.TabIndex = 15;
            this.menuButton.Text = "Menu";
            this.menuButton.UseVisualStyleBackColor = true;
            this.menuButton.Click += new System.EventHandler(this.menuButton_Click);
            // 
            // NewBookingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.menuButton);
            this.Controls.Add(this.leaveDateTimePicker);
            this.Controls.Add(this.bookButton);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.leaveLabel);
            this.Controls.Add(this.spaceTypeLabel);
            this.Controls.Add(this.spaceTypeComboBox);
            this.Controls.Add(this.startDateLabel);
            this.Controls.Add(this.startDateTimePicker);
            this.Controls.Add(this.regNoMaskedTextBox);
            this.Controls.Add(this.regNoLabel);
            this.Controls.Add(this.customerIDLabel);
            this.Controls.Add(this.customerIDMaskedTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewBookingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Airport Parking - Booking";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.NewBookingForm_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox customerIDMaskedTextBox;
        private System.Windows.Forms.Label customerIDLabel;
        private System.Windows.Forms.Label regNoLabel;
        private System.Windows.Forms.MaskedTextBox regNoMaskedTextBox;
        private System.Windows.Forms.DateTimePicker startDateTimePicker;
        private System.Windows.Forms.Label startDateLabel;
        private System.Windows.Forms.ComboBox spaceTypeComboBox;
        private System.Windows.Forms.Label spaceTypeLabel;
        private System.Windows.Forms.Label leaveLabel;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.Button bookButton;
        private System.Windows.Forms.DateTimePicker leaveDateTimePicker;
        private System.Windows.Forms.Button menuButton;
    }
}