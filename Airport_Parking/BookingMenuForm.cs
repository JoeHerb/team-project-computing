﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Airport_Parking
{
    public partial class BookingMenuForm : Form
    {
        private NewBookingForm newBookingForm;
        private AmmendBookingForm ammendBookingForm;
        private MainMenuForm mainMenuForm;

        public BookingMenuForm()
        {
            InitializeComponent();
        }

        //Exits application when close button is pressed
        private void BookingMenuForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void newBookingButton_Click(object sender, EventArgs e)
        {
            newBookingForm = new NewBookingForm();
            newBookingForm.Show();
            this.Hide();
        }

        private void menuButton_Click(object sender, EventArgs e)
        {
            mainMenuForm = new MainMenuForm();
            mainMenuForm.Show();
            this.Hide();
        }

        private void ammendBookingButton_Click(object sender, EventArgs e)
        {
            ammendBookingForm = new AmmendBookingForm();
            ammendBookingForm.Show();
            this.Hide();
        }
    }
}
