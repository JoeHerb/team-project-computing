﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.OleDb;

namespace Airport_Parking
{
    [TestClass]
    public class ConstructorsTest
    {
        [TestMethod]
        public void Customer()
        {
            OleDbConnection myConn = new OleDbConnection();
            myConn.ConnectionString = connectionDetails.dbsource;

            string forename = "Gordon";
            string surname = "Adams";
            string address = "83 Parklane Meadow";
            string telephoneNo = "07562435684";
            string email = "gordonadams@gmail.com";

            OleDbCommand myCmd = myConn.CreateCommand();
            myCmd.CommandText = "Insert Into Customer (Forename, Surname, Address, TelephoneNo, EMail)" +
                                    "Values (@forename, @surname, @address, @telephoneNo, @email)";

            myCmd.Parameters.AddWithValue("forename", forename);
            myCmd.Parameters.AddWithValue("surname", surname);
            myCmd.Parameters.AddWithValue("address", address);
            myCmd.Parameters.AddWithValue("telephoneNo", telephoneNo);
            myCmd.Parameters.AddWithValue("email", email);

            myConn.Open();
            myCmd.ExecuteNonQuery();
            myConn.Close();


            myCmd.CommandText = "Select Forename, Surname, Address, TelephoneNo, EMail From Customer"
                                + " Where CustomerID=@custID";

            myCmd.Parameters.AddWithValue("custID", "12");

            myConn.Open();
            OleDbDataReader myDR = myCmd.ExecuteReader();

            myDR.Read();

            StringAssert.Equals(forename, Convert.ToString(myDR["Forename"]));
            StringAssert.Equals(surname, Convert.ToString(myDR["Surname"]));
            StringAssert.Equals(address, Convert.ToString(myDR["Address"]));
            StringAssert.Equals(telephoneNo, Convert.ToString(myDR["TelephoneNo"]));
            StringAssert.Equals(email, Convert.ToString(myDR["EMail"]));

            myConn.Close();


        }
    }
}
