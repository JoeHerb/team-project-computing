﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Airport_Parking
{
    public partial class NewBookingForm : Form
    {
        private int totalPrice;
        private int assignedSpace;
        private bool valet;
        MainMenuForm mainMenuForm = new MainMenuForm();

        OleDbConnection myConn = new OleDbConnection();



        public NewBookingForm()
        {
            InitializeComponent();
        }

        private void NewBookingForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void bookButton_Click(object sender, EventArgs e)
        {
            bool confirmedBooking;
            assignedSpace = getFreeSpace();

            if (assignedSpace != 0)
            {
                setPrice();

                confirmedBooking = placeBooking();

                if (confirmedBooking == true)
                {
                    MessageBox.Show("Booking placed successfully.");
                    customerIDMaskedTextBox.Enabled = false;
                    regNoMaskedTextBox.Enabled = false;
                    startDateTimePicker.Enabled = false;
                    leaveDateTimePicker.Enabled = false;
                    spaceTypeComboBox.Enabled = false;
                    bookButton.Enabled = false;
                }
                else
                    MessageBox.Show("Unfortunately there was an error, please try again.");
            }
            
        }

        private void setPrice()
        {
            int dailyPrice = 0;

            if (spaceTypeComboBox.Text == "Standard")
            {
                dailyPrice = 6;
                valet = false;
            }
            else if (spaceTypeComboBox.Text == "Luxury")
            {
                dailyPrice = 7;
                valet = true;
            }
            else if (spaceTypeComboBox.Text == "Extra Secure")
            {
                dailyPrice = 8;
                valet = false;
            }

            TimeSpan duration = leaveDateTimePicker.Value - startDateTimePicker.Value;

            totalPrice = (dailyPrice * duration.Days);
            priceLabel.Text = "Total Price: £" + totalPrice;
        }

        private void spaceTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            setPrice();
        }

        private int getFreeSpace()
        {
            int space = 0;

            myConn.ConnectionString = connectionDetails.dbsource;

            OleDbCommand myCmd = myConn.CreateCommand();
            OleDbCommand myCmd2 = myConn.CreateCommand();
            OleDbCommand myCmd3 = myConn.CreateCommand();

            if (spaceTypeComboBox.SelectedIndex == 0)
                spaceTypeComboBox.Text = "Standard";
            else if (spaceTypeComboBox.SelectedIndex == 1)
                spaceTypeComboBox.Text = "Luxury";
            else if (spaceTypeComboBox.SelectedIndex == 2)
                spaceTypeComboBox.Text = "Extra Secure";

            myCmd.CommandText = @"SELECT [SpaceID]
                                FROM DesignatedSpace
                                WHERE [SpaceType]=@spaceType
                                AND [SpaceID] NOT IN (SELECT [SpaceID]
                                                      FROM [Booking])";

            myCmd.Parameters.AddWithValue("spaceType", spaceTypeComboBox.Text);

            myConn.Open();
            OleDbDataReader myDR = myCmd.ExecuteReader();

            if (myDR.HasRows)
            {
                myDR.Read();
                space = Convert.ToInt16(myDR["SpaceID"]);
                myDR.Close();
                myConn.Close();
                return space;
            }

            myCmd2.CommandText = @"SELECT [SpaceID]
                                FROM DesignatedSpace
                                WHERE [SpaceType]=@spaceType";
               
            myCmd2.Parameters.AddWithValue("spaceType", spaceTypeComboBox.Text);
            myCmd2.Parameters.AddWithValue("startDate", startDateTimePicker.Value.Date);
            myCmd2.Parameters.AddWithValue("leaveDate", leaveDateTimePicker.Value.Date);

            OleDbDataReader myDR2 = myCmd2.ExecuteReader();
            myDR2.Read();

            while (myDR2.HasRows)
            {
                try
                {
                    myCmd3.CommandText = @"SELECT [SpaceID]
                                       FROM [Booking]
                                       WHERE [SpaceID]=@spaceID
                                       AND [BookingDate] < @startDate OR @leaveDate < [BookingDate]
                                       AND [LeaveDate] < @startDate OR @leaveDate < [BookingDate]";

                    myCmd3.Parameters.AddWithValue("spaceID", myDR2["SpaceID"]);
                    myCmd3.Parameters.AddWithValue("startDate", startDateTimePicker.Value.Date);
                    myCmd3.Parameters.AddWithValue("leaveDate", leaveDateTimePicker.Value.Date);

                    OleDbDataReader myDR3 = myCmd3.ExecuteReader();

                    if (myDR3.HasRows)
                    {
                        myDR3.Read();
                        space = Convert.ToInt16(myDR3["SpaceID"]);
                        myDR3.Close();
                        myConn.Close();
                        return space;
                    }
                    else
                    {
                        myDR3.Close();
                        myConn.Close();
                    }

                    myDR2.Read();
                }
                catch
                {
                    MessageBox.Show("There were no spaces available");
                }
            }
            
            myConn.Close();
            return space;
        }

        private bool placeBooking()
        {
            try
            {
                myConn.ConnectionString = connectionDetails.dbsource;
                OleDbCommand myCmd = myConn.CreateCommand();

                myCmd.CommandText = @"Insert Into Booking (CustomerID, BookingDate, RegNo, SpaceID, LeaveDate, TotalCost, Valet)
                                      Values (@customerID, @bookingDate, @regNo, @spaceID, @leaveDate, @totalCost, @valet)";

                myCmd.Parameters.AddWithValue("customerID", customerIDMaskedTextBox.Text);
                myCmd.Parameters.AddWithValue("bookingDate", startDateTimePicker.Value.Date);
                myCmd.Parameters.AddWithValue("regNo", regNoMaskedTextBox.Text);
                myCmd.Parameters.AddWithValue("spaceID", assignedSpace);
                myCmd.Parameters.AddWithValue("leaveDate", leaveDateTimePicker.Value.Date);
                myCmd.Parameters.AddWithValue("totalCost", totalPrice);
                myCmd.Parameters.AddWithValue("valet", valet);

                myConn.Open();
                myCmd.ExecuteNonQuery();
                myConn.Close();

                return true;
            }
            catch
            {
                MessageBox.Show("There was a problem in placeBooking");
                return false;
            }
        }

        private void menuButton_Click(object sender, EventArgs e)
        {
            mainMenuForm = new MainMenuForm();
            mainMenuForm.Show();
            this.Hide();
        }
    }
}
