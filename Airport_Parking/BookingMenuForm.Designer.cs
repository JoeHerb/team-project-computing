﻿namespace Airport_Parking
{
    partial class BookingMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookingMenuForm));
            this.newBookingButton = new System.Windows.Forms.Button();
            this.ammendBookingButton = new System.Windows.Forms.Button();
            this.menuButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // newBookingButton
            // 
            this.newBookingButton.Location = new System.Drawing.Point(39, 41);
            this.newBookingButton.Name = "newBookingButton";
            this.newBookingButton.Size = new System.Drawing.Size(205, 104);
            this.newBookingButton.TabIndex = 0;
            this.newBookingButton.Text = "New Booking";
            this.newBookingButton.UseVisualStyleBackColor = true;
            this.newBookingButton.Click += new System.EventHandler(this.newBookingButton_Click);
            // 
            // ammendBookingButton
            // 
            this.ammendBookingButton.Location = new System.Drawing.Point(39, 155);
            this.ammendBookingButton.Name = "ammendBookingButton";
            this.ammendBookingButton.Size = new System.Drawing.Size(205, 104);
            this.ammendBookingButton.TabIndex = 1;
            this.ammendBookingButton.Text = "Ammend Booking";
            this.ammendBookingButton.UseVisualStyleBackColor = true;
            this.ammendBookingButton.Click += new System.EventHandler(this.ammendBookingButton_Click);
            // 
            // menuButton
            // 
            this.menuButton.Location = new System.Drawing.Point(12, 10);
            this.menuButton.Name = "menuButton";
            this.menuButton.Size = new System.Drawing.Size(48, 23);
            this.menuButton.TabIndex = 16;
            this.menuButton.Text = "Menu";
            this.menuButton.UseVisualStyleBackColor = true;
            this.menuButton.Click += new System.EventHandler(this.menuButton_Click);
            // 
            // BookingMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 271);
            this.Controls.Add(this.menuButton);
            this.Controls.Add(this.ammendBookingButton);
            this.Controls.Add(this.newBookingButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BookingMenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Airport Parking - Booking";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BookingMenuForm_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button newBookingButton;
        private System.Windows.Forms.Button ammendBookingButton;
        private System.Windows.Forms.Button menuButton;
    }
}