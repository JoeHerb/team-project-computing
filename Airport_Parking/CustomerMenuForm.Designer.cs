﻿namespace Airport_Parking
{
    partial class CustomerMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomerMenuForm));
            this.newCustomerButton = new System.Windows.Forms.Button();
            this.editCustButton = new System.Windows.Forms.Button();
            this.menuButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // newCustomerButton
            // 
            this.newCustomerButton.Location = new System.Drawing.Point(40, 45);
            this.newCustomerButton.Name = "newCustomerButton";
            this.newCustomerButton.Size = new System.Drawing.Size(205, 104);
            this.newCustomerButton.TabIndex = 0;
            this.newCustomerButton.Text = "Sign Up";
            this.newCustomerButton.UseVisualStyleBackColor = true;
            this.newCustomerButton.Click += new System.EventHandler(this.newCustomerButton_Click);
            // 
            // editCustButton
            // 
            this.editCustButton.Location = new System.Drawing.Point(40, 155);
            this.editCustButton.Name = "editCustButton";
            this.editCustButton.Size = new System.Drawing.Size(205, 104);
            this.editCustButton.TabIndex = 1;
            this.editCustButton.Text = "Edit Account Details";
            this.editCustButton.UseVisualStyleBackColor = true;
            this.editCustButton.Click += new System.EventHandler(this.editCustButton_Click);
            // 
            // menuButton
            // 
            this.menuButton.Location = new System.Drawing.Point(12, 9);
            this.menuButton.Name = "menuButton";
            this.menuButton.Size = new System.Drawing.Size(48, 23);
            this.menuButton.TabIndex = 16;
            this.menuButton.Text = "Menu";
            this.menuButton.UseVisualStyleBackColor = true;
            this.menuButton.Click += new System.EventHandler(this.menuButton_Click);
            // 
            // CustomerMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 271);
            this.Controls.Add(this.menuButton);
            this.Controls.Add(this.editCustButton);
            this.Controls.Add(this.newCustomerButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CustomerMenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Airport Parking - Customer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CustomerMenuForm_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button newCustomerButton;
        private System.Windows.Forms.Button editCustButton;
        private System.Windows.Forms.Button menuButton;
    }
}