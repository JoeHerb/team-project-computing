﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Airport_Parking
{
    public partial class CustomerMenuForm : Form
    {
        private NewCustomerForm newCustomerForm;
        private AmmendCustomerDetailsForm ammendCustomerDetailsForm;
        private MainMenuForm mainMenuForm;

        public CustomerMenuForm()
        {
            InitializeComponent();
        }

        //Exits application when close button is pressed
        private void CustomerMenuForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void newCustomerButton_Click(object sender, EventArgs e)
        {
            newCustomerForm = new NewCustomerForm();
            newCustomerForm.Show();
            this.Hide();
        }

        private void editCustButton_Click(object sender, EventArgs e)
        {
            ammendCustomerDetailsForm = new AmmendCustomerDetailsForm();
            ammendCustomerDetailsForm.Show();
            this.Hide();
        }

        private void menuButton_Click(object sender, EventArgs e)
        {
            mainMenuForm = new MainMenuForm();
            mainMenuForm.Show();
            this.Hide();
        }
    }
}
