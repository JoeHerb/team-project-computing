﻿namespace Airport_Parking
{
    partial class AmmendCustomerDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AmmendCustomerDetailsForm));
            this.customerIDLabel = new System.Windows.Forms.Label();
            this.customerIDMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.searchRecordsButton = new System.Windows.Forms.Button();
            this.forenameLabel = new System.Windows.Forms.Label();
            this.surnameLabel = new System.Windows.Forms.Label();
            this.addressLabel = new System.Windows.Forms.Label();
            this.phoneNoLabel = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.forenameMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.surnameMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.phoneNoMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.customerDetailsGroupBox = new System.Windows.Forms.GroupBox();
            this.updateButton = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.menuButton = new System.Windows.Forms.Button();
            this.deleteCustomerButton = new System.Windows.Forms.Button();
            this.customerDetailsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // customerIDLabel
            // 
            this.customerIDLabel.AutoSize = true;
            this.customerIDLabel.Location = new System.Drawing.Point(81, 51);
            this.customerIDLabel.Name = "customerIDLabel";
            this.customerIDLabel.Size = new System.Drawing.Size(130, 13);
            this.customerIDLabel.TabIndex = 0;
            this.customerIDLabel.Text = "Please enter Customer ID:";
            // 
            // customerIDMaskedTextBox
            // 
            this.customerIDMaskedTextBox.Location = new System.Drawing.Point(217, 48);
            this.customerIDMaskedTextBox.Mask = "000000000";
            this.customerIDMaskedTextBox.Name = "customerIDMaskedTextBox";
            this.customerIDMaskedTextBox.Size = new System.Drawing.Size(103, 20);
            this.customerIDMaskedTextBox.TabIndex = 1;
            // 
            // searchRecordsButton
            // 
            this.searchRecordsButton.Location = new System.Drawing.Point(187, 308);
            this.searchRecordsButton.Name = "searchRecordsButton";
            this.searchRecordsButton.Size = new System.Drawing.Size(75, 23);
            this.searchRecordsButton.TabIndex = 12;
            this.searchRecordsButton.Text = "Search";
            this.searchRecordsButton.UseVisualStyleBackColor = true;
            this.searchRecordsButton.Click += new System.EventHandler(this.searchRecordsButton_Click);
            // 
            // forenameLabel
            // 
            this.forenameLabel.AutoSize = true;
            this.forenameLabel.Location = new System.Drawing.Point(48, 16);
            this.forenameLabel.Name = "forenameLabel";
            this.forenameLabel.Size = new System.Drawing.Size(57, 13);
            this.forenameLabel.TabIndex = 2;
            this.forenameLabel.Text = "Forename:";
            // 
            // surnameLabel
            // 
            this.surnameLabel.AutoSize = true;
            this.surnameLabel.Location = new System.Drawing.Point(47, 49);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(52, 13);
            this.surnameLabel.TabIndex = 3;
            this.surnameLabel.Text = "Surname:";
            // 
            // addressLabel
            // 
            this.addressLabel.AutoSize = true;
            this.addressLabel.Location = new System.Drawing.Point(48, 71);
            this.addressLabel.Name = "addressLabel";
            this.addressLabel.Size = new System.Drawing.Size(48, 13);
            this.addressLabel.TabIndex = 4;
            this.addressLabel.Text = "Address:";
            // 
            // phoneNoLabel
            // 
            this.phoneNoLabel.AutoSize = true;
            this.phoneNoLabel.Location = new System.Drawing.Point(47, 134);
            this.phoneNoLabel.Name = "phoneNoLabel";
            this.phoneNoLabel.Size = new System.Drawing.Size(58, 13);
            this.phoneNoLabel.TabIndex = 5;
            this.phoneNoLabel.Text = "Phone No:";
            // 
            // emailLabel
            // 
            this.emailLabel.AutoSize = true;
            this.emailLabel.Location = new System.Drawing.Point(48, 160);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(39, 13);
            this.emailLabel.TabIndex = 6;
            this.emailLabel.Text = "E-Mail:";
            // 
            // forenameMaskedTextBox
            // 
            this.forenameMaskedTextBox.Location = new System.Drawing.Point(133, 16);
            this.forenameMaskedTextBox.Mask = "LLLLLLLLLLLLLLL";
            this.forenameMaskedTextBox.Name = "forenameMaskedTextBox";
            this.forenameMaskedTextBox.Size = new System.Drawing.Size(103, 20);
            this.forenameMaskedTextBox.TabIndex = 7;
            // 
            // surnameMaskedTextBox
            // 
            this.surnameMaskedTextBox.Location = new System.Drawing.Point(133, 42);
            this.surnameMaskedTextBox.Mask = "LLLLLLLLLLLLLLL";
            this.surnameMaskedTextBox.Name = "surnameMaskedTextBox";
            this.surnameMaskedTextBox.Size = new System.Drawing.Size(103, 20);
            this.surnameMaskedTextBox.TabIndex = 8;
            // 
            // phoneNoMaskedTextBox
            // 
            this.phoneNoMaskedTextBox.Location = new System.Drawing.Point(133, 127);
            this.phoneNoMaskedTextBox.Mask = "00000000000";
            this.phoneNoMaskedTextBox.Name = "phoneNoMaskedTextBox";
            this.phoneNoMaskedTextBox.Size = new System.Drawing.Size(103, 20);
            this.phoneNoMaskedTextBox.TabIndex = 9;
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(133, 153);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(103, 20);
            this.emailTextBox.TabIndex = 10;
            // 
            // addressTextBox
            // 
            this.addressTextBox.Location = new System.Drawing.Point(133, 68);
            this.addressTextBox.Multiline = true;
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.Size = new System.Drawing.Size(103, 52);
            this.addressTextBox.TabIndex = 11;
            // 
            // customerDetailsGroupBox
            // 
            this.customerDetailsGroupBox.Controls.Add(this.deleteCustomerButton);
            this.customerDetailsGroupBox.Controls.Add(this.addressTextBox);
            this.customerDetailsGroupBox.Controls.Add(this.updateButton);
            this.customerDetailsGroupBox.Controls.Add(this.emailTextBox);
            this.customerDetailsGroupBox.Controls.Add(this.phoneNoMaskedTextBox);
            this.customerDetailsGroupBox.Controls.Add(this.surnameMaskedTextBox);
            this.customerDetailsGroupBox.Controls.Add(this.forenameMaskedTextBox);
            this.customerDetailsGroupBox.Controls.Add(this.emailLabel);
            this.customerDetailsGroupBox.Controls.Add(this.phoneNoLabel);
            this.customerDetailsGroupBox.Controls.Add(this.addressLabel);
            this.customerDetailsGroupBox.Controls.Add(this.surnameLabel);
            this.customerDetailsGroupBox.Controls.Add(this.forenameLabel);
            this.customerDetailsGroupBox.Enabled = false;
            this.customerDetailsGroupBox.Location = new System.Drawing.Point(84, 74);
            this.customerDetailsGroupBox.Name = "customerDetailsGroupBox";
            this.customerDetailsGroupBox.Size = new System.Drawing.Size(285, 228);
            this.customerDetailsGroupBox.TabIndex = 13;
            this.customerDetailsGroupBox.TabStop = false;
            this.customerDetailsGroupBox.Text = "Details";
            this.customerDetailsGroupBox.Visible = false;
            // 
            // updateButton
            // 
            this.updateButton.Enabled = false;
            this.updateButton.Location = new System.Drawing.Point(50, 191);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(75, 23);
            this.updateButton.TabIndex = 14;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // menuButton
            // 
            this.menuButton.Location = new System.Drawing.Point(12, 12);
            this.menuButton.Name = "menuButton";
            this.menuButton.Size = new System.Drawing.Size(48, 23);
            this.menuButton.TabIndex = 16;
            this.menuButton.Text = "Menu";
            this.menuButton.UseVisualStyleBackColor = true;
            this.menuButton.Click += new System.EventHandler(this.menuButton_Click);
            // 
            // deleteCustomerButton
            // 
            this.deleteCustomerButton.Location = new System.Drawing.Point(158, 189);
            this.deleteCustomerButton.Name = "deleteCustomerButton";
            this.deleteCustomerButton.Size = new System.Drawing.Size(78, 25);
            this.deleteCustomerButton.TabIndex = 31;
            this.deleteCustomerButton.Text = "Delete";
            this.deleteCustomerButton.UseVisualStyleBackColor = true;
            this.deleteCustomerButton.Click += new System.EventHandler(this.deleteCustomerButton_Click);
            // 
            // AmmendCustomerDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 343);
            this.Controls.Add(this.menuButton);
            this.Controls.Add(this.customerDetailsGroupBox);
            this.Controls.Add(this.searchRecordsButton);
            this.Controls.Add(this.customerIDMaskedTextBox);
            this.Controls.Add(this.customerIDLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AmmendCustomerDetailsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Airport Parking - Customer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AmmendCustomerDetailsForm_FormClosed);
            this.customerDetailsGroupBox.ResumeLayout(false);
            this.customerDetailsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label customerIDLabel;
        private System.Windows.Forms.MaskedTextBox customerIDMaskedTextBox;
        private System.Windows.Forms.Button searchRecordsButton;
        private System.Windows.Forms.Label forenameLabel;
        private System.Windows.Forms.Label surnameLabel;
        private System.Windows.Forms.Label addressLabel;
        private System.Windows.Forms.Label phoneNoLabel;
        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.MaskedTextBox forenameMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox surnameMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox phoneNoMaskedTextBox;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.GroupBox customerDetailsGroupBox;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Button menuButton;
        private System.Windows.Forms.Button deleteCustomerButton;
    }
}