﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.Data.OleDb;

namespace Airport_Parking
{
    public partial class NewCustomerForm : Form
    {
        private MainMenuForm mainMenuForm;
        OleDbConnection myConn = new OleDbConnection();
        
        public NewCustomerForm()
        {
            InitializeComponent();
        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            myConn.ConnectionString = connectionDetails.dbsource;

            if (CheckEmail()==true)
            {
                errorProvider.SetError(emailTextBox, String.Empty);

                OleDbCommand myCmd = myConn.CreateCommand();
                myCmd.CommandText = "Insert Into Customer (Forename, Surname, Address, TelephoneNo, EMail)" +
                                        "Values (@forename, @surname, @address, @telephoneNo, @email)";

                myCmd.Parameters.AddWithValue("forename", forenameMaskedTextBox.Text);
                myCmd.Parameters.AddWithValue("surname", surnameMaskedTextBox.Text);
                myCmd.Parameters.AddWithValue("address", addressTextBox.Text);
                myCmd.Parameters.AddWithValue("telephoneNo", phoneNoMaskedTextBox.Text);
                myCmd.Parameters.AddWithValue("email", emailTextBox.Text);

                myConn.Open();
                myCmd.ExecuteNonQuery();
                myConn.Close();
            }
            else
            {
                errorProvider.SetError(emailTextBox, "Please enter a valid email address");
            }
        }

        private bool CheckEmail()
        {
            try
            {
                MailAddress m = new MailAddress(emailTextBox.Text);
                return true;
            }
            catch 
            {
                return false;
            }
        }

        private void NewCustomerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void menuButton_Click(object sender, EventArgs e)
        {
            mainMenuForm = new MainMenuForm();
            mainMenuForm.Show();
            this.Hide();
        }
    }
}
